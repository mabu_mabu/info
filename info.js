const userInfos = [
  {
    account: 'aqdemo1',
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZW1iZXJJRCI6ImFxZGVtbzFAYXFkZW1vLnNhbGVzYWNjIiwiZXhwIjoxNjE2ODA5NzM4MzE3LCJpYXQiOjE2MTY3MjMzMzgzMTd9.gsx6GNIeUoYbxIgy7_rrYBe2l-Q6mjityB0LHyaZ1VU',
  },
  {
    account: 'aqdemo2',
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZW1iZXJJRCI6ImFxZGVtbzJAYXFkZW1vLnNhbGVzYWNjIiwiZXhwIjoxNjE2ODA5NzM4ODE0LCJpYXQiOjE2MTY3MjMzMzg4MTR9.iqL5e7S89_Ke-Vta_Gb1mKimB_upr8-UlVxcwoQ9bpo',
  },
  {
    account: 'aqdemo3',
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZW1iZXJJRCI6ImFxZGVtbzNAYXFkZW1vLnNhbGVzYWNjIiwiZXhwIjoxNjE2ODA5NzM5MzE3LCJpYXQiOjE2MTY3MjMzMzkzMTd9.FBbfOkP0BXnQGNgRaBtIXLg6jIZa5QgrIgHliXW3isQ',
  },
  {
    account: 'aqdemo4',
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZW1iZXJJRCI6ImFxZGVtbzRAYXFkZW1vLnNhbGVzYWNjIiwiZXhwIjoxNjE2ODA5NzM5ODIyLCJpYXQiOjE2MTY3MjMzMzk4MjJ9.6bIK-dIXrUDQfifPb3973eb5CsiHsSwKQj6uv34EAkE',
  },
]
